/* Sьntese
    - Objetivo: Programa para simular o gerenciamento de memзria de um SO
    - Entradas: Tamanho da memзria, Tamanho da pрgina, Nome do programa, Tamanho do programa
    - Saьdas: Tabela (Gerenciador de tarefas), Representaусo Grрfica
*/
 
/* Funушes do programa
    - defineMemoria(int tamanho_memoria, int tamanho_pрgina)
    - int runPrograma(char * nome_do_programa, int tamanho_memoria_programa)
        * Deve retornar um "ID" do programa, para ser identificado posteriormente
    - killPrograma(int ID_do_programa)
    - gerenciadorDeTarefas(void)
        * Mostra uma tabela com os processos em execuусo
        * Ao final, mostra o total de memзria usada e total de memзria livre
    - runDefrag(void)
        * Essa funусo estр responsрvel para "organizar" a memзria utilizada
        * Antes:    Memзria: |x|x| |x| | |
        * Depois:   Memзria: |x|x|x| | | |
*/
 
/* Emails para contato
    - matheus_oliveira30@hotmail.com
    - matheussbernardo@gmail.com
    - renata.sds09@gmail.com
*/
 
#include <stdio.h>
#include <stdlib.h>
/* Incluir header para grрficos */
 
 
/* Definiусo de um "programa" */
typedef struct processo
{
    char * nome_do_programa;
    int ID_programa;
    int tamanho_necessario;
} programa;
 
/* Definiусo da lista */
typedef struct node
{
    programa * processo_rodando;
    /* Deve ser NULL se nсo tiver ninguжm rodando */
    /* Caso tenha algum processo rodando, deve possuir um ponteiro com as informaушes do programa em execuусo*/
 
    /* Variрveis para indicar o total de memзria por nз */
    int tamanho_utilizado;
    int tamanho_maximo;
 
    struct node * anterior;
    struct node * proximo;
} pagina_memoria;
 
/* Definiусo do cabeуalho */
typedef struct header
{
    /* Apontadores para inьcio e fim da memзria */
    pagina_memoria * inicio;
    pagina_memoria * fim;
 
    /* Variрveis de uso da memзria */
    int total_utilizado, total_livre, total;
 
    /* Variрvel para indicar a primeira pрgina livre */
    pagina_memoria * primeira_livre;
} header_memoria;
 
/* Funусo para gerar processo */
programa * gerarNovoProcesso(char * nome, int ID, int tamanho)
{
 
}
 
/* Funусo para gerar uma pрgina */
pagina_memoria * gerarNovaPagina(int tamanho_max)
{
    pagina_memoria * novaPagina = (pagina_memoria*)malloc(sizeof(pagina_memoria));
 
    novaPagina->anterior = NULL;
    novaPagina->proximo = NULL;
 
    novaPagina->processo_rodando = NULL;
 
    novaPagina->tamanho_utilizado = 0;
    novaPagina->tamanho_maximo = tamanho_max;
 
    return novaPagina;
 
}
 
/* Funусo para inserir na frente */
void inserirFrente(header_memoria * head, int tamanho_pag)
{
    pagina_memoria * novoNo = gerarNovaPagina(tamanho_pag);
 
    if(head->inicio == NULL)
    {
        head->inicio = novoNo;
        head->fim = novoNo;
 
        (head->inicio)->proximo = head->fim;
        (head->inicio)->anterior = head->fim;
 
        (head->fim)->proximo = head->inicio;
        (head->fim)->anterior = head->inicio;
    }
    else
    {
        novoNo->proximo = head->inicio;
        novoNo->anterior = head->fim;
 
        head->inicio = novoNo;
        (head->fim)->proximo = head->inicio;
        (head->inicio)->anterior = head->fim;
    }
 
}
 
/* Funусo para definir a memзria */
header_memoria * definirMemoria(int tamanho_max, int tamanho_pag, header_memoria * memoria)
{
    int contador, particoes;
 
    /* Define o nЩmero de partiушes da memзria */
    if(tamanho_max % 2 == 0 && tamanho_pag % 2 == 0)
    {
        particoes = tamanho_max / tamanho_pag;
    }
    else
    {
        printf("Erro - Memoria nao particionavel!");
        return NULL;
    }
 
    /* Cria partiушes e a memзria */
    for (contador = 0; contador < particoes; contador++)
    {
        inserirFrente(memoria, tamanho_pag);
    }
}
 
header_memoria * iniciarMemoria(void)
{
    header_memoria * novaMemoria = (header_memoria*)malloc(sizeof(header_memoria));
 
    novaMemoria->inicio = NULL;
    novaMemoria->fim = NULL;
 
    novaMemoria->primeira_livre = NULL;
 
    novaMemoria->total = 0;
    novaMemoria->total_livre = 0;
    novaMemoria->total_utilizado = 0;
 
    return (novaMemoria);
}
 
void imprimirZeros(header_memoria * memoria)
{
    pagina_memoria * contador = memoria->inicio;
    int numero = 0;
 
    while (contador != memoria->fim)
    {
        printf("Bloco #%d = %d\n", numero, contador->tamanho_maximo);
        contador = contador->proximo;
 
        numero++;
 
    }
}
 
 
int main(void)
{
    header_memoria * head = iniciarMemoria();
 
    definirMemoria(100, 10, head);
 
    imprimirZeros(head);
 
 
 
 
 
 
 
    return 0;
}